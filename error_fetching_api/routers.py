from rest_framework.routers import DefaultRouter
from error_api.viewset import ErrorItemsViewset

router=DefaultRouter()
router.register('ErrorItems',ErrorItemsViewset)
