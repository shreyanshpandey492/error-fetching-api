1- This api is used to fetch the error data that is present in psql database.
2- Credentials of the database is provided in the settings.py and can be changed.
3- The api utilizes the following URLs
      http://localhost/8000/api/erroritems----- to display all the records present in the database
      http://localhost/8000/api/erroritems/<pk> ---- To access the record with a particular primary key
4- The api also includes a search function that helps u filter out records with the input searched text
5- put,post and delete operations can be executed by using the keys on the page, else postman can be used to hit these operations
   with any of the above verbs.
6-This project utilises ErrorItems model to store data into the database.
