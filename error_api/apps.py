from django.apps import AppConfig


class ErrorApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'error_api'
