from rest_framework import viewsets
from . import models
from . import serializers
from rest_framework import status,filters


class ErrorItemsViewset(viewsets.ModelViewSet):
    queryset = models.ErrorItems.objects.all()
    serializer_class = serializers.ErrorItemsSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('error',)
